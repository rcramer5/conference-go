import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f"{city} {state}", "per_page": 1}
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    # Make the request
    r = requests.get(url, params=params, headers=headers)
    # Parse the JSON response
    json_response = r.json()
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    purl = json_response["photos"][0]["src"]["original"]
    pdict = {"picture_url": purl}
    return pdict


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    gurl = "http://api.openweathermap.org/geo/1.0/direct"
    gparams = {
        "appid": OPEN_WEATHER_API_KEY,
        "q": f"{city},{state},USA",
    }
    # Make the request
    l = requests.get(gurl, params=gparams)
    # Parse the JSON response
    geo_response = l.json()
    # Get the latitude and longitude from the response
    latitude = geo_response[0]["lat"]
    longitude = geo_response[0]["lon"]
    # Create the URL for the current weather API with the latitude
    wparams = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
    }
    wurl = "https://api.openweathermap.org/data/2.5/weather"
    #   and longitude
    # Make the request
    r = requests.get(wurl, params=wparams)
    # Parse the JSON response
    weth_response = r.json()
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    description = weth_response["weather"][0]["description"]
    temp = weth_response["main"]["temp"]
    wdict = {"description": description, "temperature": temp}
    # Return the dictionary
    return wdict
